// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "@le7el/web3_crs/contracts/controller/GatedController.sol";

contract WorldControllerV1 is GatedController {
    constructor (IBaseNFT _base) GatedController(_base) {}
}