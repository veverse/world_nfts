// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "@le7el/web3_crs/contracts/nft/BaseNFT.sol";
import "@le7el/web3_crs/contracts/resolver/profile/ITextResolver.sol";

contract WorldNFTV1 is BaseNFT {
    constructor(ICRS _crs, bytes32 _baseNode) ERC721("VeVerse World v1", "WORLD") {
        crs = _crs;
        baseNode = _baseNode;
    }

    /**
     * @dev NFT metadata.
     * @param _id The token ID (keccak256 of the label).
     * @return Metadata URI or base64 encoded metadata.
     */
    function tokenURI(uint256 _id) public override(BaseNFT) view returns (string memory) {
        bytes32 _baseNode = baseNode;
        ICRS _crs = crs;
        address _proxy = IProxyConfigResolver(_crs.resolver(_baseNode)).proxyConfig(_baseNode, address(this), TOKEN_URI_SELECTOR);
        bytes32 _nodehash = keccak256(abi.encodePacked(_baseNode, bytes32(_id)));
        
        string memory _customAvatar = "";
        address _resolver = _crs.resolver(_nodehash);
        if (_resolver != address(0)) _customAvatar = ITextResolver(_resolver).text(_nodehash, "VEVERSE_WORLD_IMAGE");
        return ITokenURIProxy(_proxy).tokenURI(_id, names[_id], _customAvatar);
    }
}
