// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

interface IManagedResolver {
    event RoleChanged(
        bytes32 indexed node,
        bytes4 indexed roleSig,
        address indexed manager,
        bool active
    );

    /**
     * @dev Check if manager address has some role.
     * @param _node the node to update.
     * @param _roleSig bytes4 signature of a role generated like bytes4(keccak256("ROLE_NAME")).
     * @param _manager address which will get the role.
     * @return true if manager address has role.
     */
    function hasRole(bytes32 _node, bytes4 _roleSig, address _manager) external view returns (bool);
}