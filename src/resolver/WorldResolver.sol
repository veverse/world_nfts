// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "@le7el/web3_crs/contracts/registry/ICRS.sol";
import "@le7el/web3_crs/contracts/resolver/IResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/AddressResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/ContentHashResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/NameResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/TextResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/InterfaceResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/ABIResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/PubkeyResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/RoyaltiesResolver.sol";
import "@le7el/web3_crs/contracts/resolver/profile/ProxyConfigResolver.sol";
import "@le7el/web3_crs/contracts/resolver/Multicallable.sol";
import "./profile/ManagedResolver.sol";

contract WorldResolver is 
    Multicallable,
    AddressResolver,
    ContentHashResolver,
    NameResolver,
    TextResolver,
    InterfaceResolver,
    ABIResolver,
    PubkeyResolver,
    RoyaltiesResolver,
    ProxyConfigResolver,
    ManagedResolver
{
    ICRS crs;

    /**
     * A mapping of operators. An address that is authorised for an address
     * may make any changes to the name that the owner could, but may not update
     * the set of authorisations.
     * (owner, operator) => approved
     */
    mapping(address => mapping(address => bool)) private _operatorApprovals;

    // Logged when an operator is added or removed.
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    constructor(ICRS _crs) {
        crs = _crs;
    }

    /**
     * @dev See {IERC1155-setApprovalForAll}.
     */
    function setApprovalForAll(address operator, bool approved) external {
        require(
            msg.sender != operator,
            "ERC1155: setting approval status for self"
        );

        _operatorApprovals[msg.sender][operator] = approved;
        emit ApprovalForAll(msg.sender, operator, approved);
    }

    function isAuthorised(bytes32 node) internal override view returns(bool) {
        address owner = crs.owner(node);
        return owner == msg.sender || isApprovedForAll(owner, msg.sender);
    }

    /**
     * @dev Allow 3rd parties to store isolated records for the node.
     * @param controller 3rd party controller of metadata.
     * @return true if access is authorized.
     */
    function isDelegated(bytes32, address controller) internal override view returns(bool) {
        return controller == msg.sender;
    }

    /**
     * @dev See {IERC1155-isApprovedForAll}.
     */
    function isApprovedForAll(address account, address operator) public view returns (bool) {
        return _operatorApprovals[account][operator];
    }

    function supportsInterface(bytes4 interfaceID) public override(
        Multicallable,
        AddressResolver,
        ContentHashResolver,
        NameResolver,
        TextResolver,
        InterfaceResolver,
        ABIResolver,
        PubkeyResolver,
        RoyaltiesResolver,
        ProxyConfigResolver,
        ManagedResolver
    ) pure returns(bool) {
        return interfaceID == type(IMulticallable).interfaceId || super.supportsInterface(interfaceID);
    }
}