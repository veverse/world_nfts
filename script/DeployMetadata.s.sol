// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Script.sol";

import {WorldMetadataProxyV1} from "src/nft/WorldMetadataProxyV1.sol";
import {WorldControllerV1} from "src/controller/WorldControllerV1.sol";

contract DeployMetadataScript is Script {
    WorldMetadataProxyV1 internal metadata;
    WorldControllerV1 internal controller;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address controller_address = vm.envAddress("CONTROLLER_ADDRESS");
        require(controller_address != address(0), "set CONTROLLER_ADDRESS env variable");

        controller = WorldControllerV1(payable(controller_address));
        metadata = new WorldMetadataProxyV1();
        console.log("WorldMetadataProxyV1: ");
        console.log(address(metadata));
        controller.changeTokenURIProxy(address(metadata));
        vm.stopBroadcast();
    }
}
