// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Script.sol";

import "@le7el/web3_crs/contracts/registry/CRSRegistry.sol";
import "@le7el/web3_crs/contracts/nft/IBaseNFT.sol";
import {WorldControllerV1} from "src/controller/WorldControllerV1.sol";
import {WorldNFTV1} from "src/nft/WorldNFTV1.sol";
import {WorldResolver} from "src/resolver/WorldResolver.sol";

contract DeployAllScript is Script {
    bytes32 internal xrBasenode = 0xe110fd4d72744cb6a79e0477d9fe5389d43c86bc065fc9f87570df8590e07285;
    bytes32 internal xrHash = 0x3ea00ad658f4efcf0205eb4b3b3d56898b505e5ca0f0e0f8ffa09f87ce5d88ca;
    CRSRegistry internal crs;
    WorldResolver internal resolver;
    WorldNFTV1 internal nft;
    WorldControllerV1 internal controller;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address crs_address = vm.envAddress("CRS_REGISTRY_ADDRESS");
        require(crs_address != address(0), "set CRS_REGISTRY_ADDRESS env variable");
        crs = CRSRegistry(crs_address);

        nft = new WorldNFTV1(crs, xrBasenode);
        console.log("World NFT: ");
        console.log(address(nft));
        controller = new WorldControllerV1(nft);
        console.log("World NFT controller: ");
        console.log(address(controller));
        nft.addController(address(controller));

        address resolver_address = vm.envAddress("RESOLVER_ADDRESS");
        if (resolver_address != address(0)) {
            resolver = WorldResolver(resolver_address);
        } else {
            resolver = new WorldResolver(crs);
            console.log("Resolver: ");
            console.log(address(resolver));
        }

        // Will fail if you try to override one of existing zones
        crs.setSubnodeOwner(bytes32(0), xrHash, msg.sender);
        crs.setResolver(xrBasenode, address(resolver));
        resolver.setRoyalties(xrBasenode, address(controller), 3168873850, address(0), address(0)); // 1 gas coin per year
        crs.setSubnodeOwner(bytes32(0), xrHash, address(nft));
        vm.stopBroadcast();
    }
}
