const path = require('path');

module.exports = (env, argv) => { 
  return {
    mode: argv.mode === 'production' ? 'production' : 'development',
    devtool: 'inline-source-map',
    devServer: {
      static: './demo'
    },
    entry: {
      index: './js/index.js',
    },
    output: {
      path: path.resolve(__dirname, 'demo'),
      filename: '[name].js',
      library: {
        name: 'core_nfts',
        type: 'umd',
      }
    }
  }
}