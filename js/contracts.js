export default {
  '4': {
    'v1': {
      registry: "0xf754b8De91Bd619227B5DF74c760B58048A4095D",
      resolver: "0xc71ff4deab4ba2e99e93e8406cf2fa252dd14545",
      controller: "0x4230c3cbd66e62e96ca8d997809e0c99b9615683",
      nft: "0xdd32abef08ec0fa24d169a990cff1ee81bfa1bc0"
    }
  }
}