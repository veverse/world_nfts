import {
  utils,
  rentPrice,
  domainSeparator,
  makeCommitment,
  commit,
  register,
  setText,
  getText,
  grantRole,
  revokeRole,
  hasRole,
  getNftsForWallet
} from './lib'

const {erc20Approve, nameToNode} = utils

const CHAIN_ID = '4'
const ADDRESS_ZERO = '0x0000000000000000000000000000000000000000'
const PRIV_KEY = '888fa71d782f31e9d1c952ab74d23a0f8f3f4dc189b8165a94810cf62c805af8' // '0x11169009E2E4956205632177ba1d2F2603342D91'

const whitelist = async function(address, name) {
  const whitelistMessage = ethers.utils.keccak256(
      ethers.utils.defaultAbiCoder.encode(
          ["bytes32", "address", "string"], [
              await domainSeparator(CHAIN_ID),
              address,
              name
          ],
      ),
  )

  const signer = new ethers.Wallet(PRIV_KEY)
  const whitelistSignature = await signer.signMessage(ethers.utils.arrayify(whitelistMessage))
  const sig = ethers.utils.splitSignature(
      ethers.utils.arrayify(whitelistSignature)
  );
  const whitelistSignatureEncoded = ethers.utils.defaultAbiCoder.encode(
      ["uint8", "bytes32", "bytes32"], [sig.v, sig.r, sig.s],
  )
  return whitelistSignatureEncoded
}

document.getElementById('crs_commit').addEventListener('click', () => {
  const controller = document.getElementById('crs_controller').value
  const name = document.getElementById('crs_commit_name').value
  if (name.length <= 2) return

  let registrant;
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const secret = ethers.utils.randomBytes(32)
  document.getElementById('crs_secret').value = ethers.utils.hexlify(secret)

  provider.listAccounts()
    .then((accounts) => {
      registrant = accounts[0];
      return makeCommitment(name, registrant, secret, CHAIN_ID)
    })
    .then((commitment) => {
      document.getElementById('crs_reg_name').value = name;
      document.getElementById('crs_commitment').value = commitment;
      return whitelist(registrant, name)
        .then((pass) => {
          return commit(name, commitment, pass, CHAIN_ID)
        })
    })
    .then((tr) => {
      document.getElementById('crs_commit_mining').style.display = 'block';
      return provider.waitForTransaction(tr.hash)
    })
    .then(() => {
      document.getElementById('crs_commit_mining').style.display = 'none';
      const timerNode = document.getElementById('crs_maturation');
      timerNode.parentNode.style.display = 'block';
      let timer;
      timer = setInterval(() => {
        timerNode.innerHTML = parseInt(timerNode.innerHTML) - 1;
        if (timerNode.innerHTML === '0') {
          document.getElementById('crs_commit').disabled = true;
          document.getElementById('crs_register').disabled = false;
          timerNode.parentNode.style.display = 'none';
          clearInterval(timer);
        }
      }, 1000);
    })
})

document.getElementById('crs_register').addEventListener('click', () => {
  const controller = document.getElementById('crs_controller').value
  const name = document.getElementById('crs_commit_name').value
  if (name.length <= 2) return

  const commitment = document.getElementById('crs_commitment').value
  if (!commitment) return

  document.getElementById('crs_done').innerHTML = 'Please, refresh the page to register a new NFT';
  document.getElementById('crs_done').style.display = 'none';

  const secret = document.getElementById('crs_secret').value
  const duration = document.getElementById('crs_duration').value

  const provider = new ethers.providers.Web3Provider(window.ethereum)
  provider.listAccounts()
    .then((accounts) => {
      return rentPrice(name, duration, CHAIN_ID)
        .then((response) => {
          const token = response[0]
          const amount = response[1]
          if (token === ADDRESS_ZERO || response[1].isZero()) {
            return register(name, accounts[0], duration, secret, CHAIN_ID)
          } else {
            return assertContractDeployed(CHAIN_ID, controller, 'v1')
              .then((spender) => {
                return erc20Approve(token, spender, amount)
                  .then(tr => provider.waitForTransaction(tr.hash))
                  .then(_ => register(name, accounts[0], duration, secret, CHAIN_ID))
              })
          }
        })
    })
    .then(() => {
      document.getElementById('crs_done').style.display = 'block';
    })
    .catch((error) => {
      document.getElementById('crs_done').innerHTML = error;
      document.getElementById('crs_done').style.display = 'block';
    })
})

function debounce(func, timeout = 300) {
  let timer
  return (...args) => {
      clearTimeout(timer)
      timer = setTimeout(() => { func.apply(this, args) }, timeout)
  }
}

const checkRecord = (event) => {
  const value = event.target.value
  if (!value) {
    document.getElementById('crs_manage_get').disabled = "disabled"
    document.getElementById('crs_manage_set').disabled = "disabled"
    document.getElementById('crs_manage_role_check').disabled = "disabled"
    document.getElementById('crs_manage_role_grant').disabled = "disabled"
    document.getElementById('crs_manage_role_revoke').disabled = "disabled"
    return
  }

  document.getElementById('crs_manage_get').disabled = false
  document.getElementById('crs_manage_set').disabled = false
  document.getElementById('crs_manage_role_check').disabled = false
  document.getElementById('crs_manage_role_grant').disabled = false
  document.getElementById('crs_manage_role_revoke').disabled = false
}
document.getElementById('crs_manage_name').addEventListener('change', debounce(checkRecord))

document.getElementById('crs_manage_set').addEventListener('click', () => {
  const key = document.getElementById('crs_manage_text_key').value
  const value = document.getElementById('crs_manage_text_value').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!key || !value || !name) return 

  setText(node, key, value, CHAIN_ID)
})

document.getElementById('crs_manage_get').addEventListener('click', () => {
  const key = document.getElementById('crs_manage_text_key').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!key || !name) return 

  getText(node, key, CHAIN_ID)
    .then((value) => document.getElementById('crs_manage_text_value').value = value)
})

document.getElementById('crs_manage_role_check').addEventListener('click', () => {
  const role = document.getElementById('crs_manage_role_name').value
  const manager = document.getElementById('crs_manage_role_address').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!role || !manager || !name) return 

  hasRole(node, role, manager, CHAIN_ID)
    .then((value) => document.getElementById('crs_manage_role_result').value = value)
})

document.getElementById('crs_manage_role_grant').addEventListener('click', () => {
  const role = document.getElementById('crs_manage_role_name').value
  const manager = document.getElementById('crs_manage_role_address').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!role || !manager || !name) return 

  grantRole(node, role, manager, CHAIN_ID)
})

document.getElementById('crs_manage_role_revoke').addEventListener('click', () => {
  const role = document.getElementById('crs_manage_role_name').value
  const manager = document.getElementById('crs_manage_role_address').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!role || !manager || !name) return 

  revokeRole(node, role, manager, CHAIN_ID)
})