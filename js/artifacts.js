import registry from "../out/CRSRegistry.sol/CRSRegistry.json"
import worldControllerV1 from "../out/WorldControllerV1.sol/WorldControllerV1.json"
import worldNFTV1 from "../out/WorldNFTV1.sol/WorldNFTV1.json"
import worldResolver from "../out/WorldResolver.sol/WorldResolver.json"

export default {
  registry,
  worldResolver,
  worldControllerV1,
  worldNFTV1
}