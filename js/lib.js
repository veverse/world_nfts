import artifacts from './artifacts'
import contracts from './contracts'
import {ethers, controller, resolver, nft, utils} from '@le7el/web3_crs'

const CURRENT_VERSION = 'v1'

const assertContractDeployed = (chainId, contractName, version) => {
  if (contracts[`${chainId}`] && contracts[`${chainId}`][version] && contracts[`${chainId}`][version][contractName]) {
    return Promise.resolve(contracts[chainId][version][contractName])
  }
  return Promise.reject(`contract ${contractName}:${version} is not deployed to network ${chainId}`);
}

const wrapControllerCall = (chainId, contract, fnName, params) =>
  assertContractDeployed(chainId, contract, CURRENT_VERSION)
    .then((contractAddress) => {
      params.push(contractAddress)
      return controller[fnName].apply(this, params)
    })

const wrapResolverCall = (chainId, contract, fnName, params) =>
  assertContractDeployed(chainId, contract, CURRENT_VERSION)
    .then((contractAddress) => {
      params.push(contractAddress)
      return resolver[fnName].apply(this, params)
    })

const wrapNftCall = (chainId, contract, fnName, params) =>
  assertContractDeployed(chainId, contract, CURRENT_VERSION)
    .then((contractAddress) => {
      params.push(contractAddress)
      return nft[fnName].apply(this, params)
    })

const initResolverContract = (chainId, readOnly = true) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((contract_address) => utils.initContract(contract_address, artifacts.worldResolver.abi, readOnly))

const initNftContract = (chainId, readOnly = true) =>
  assertContractDeployed(chainId, 'nft', CURRENT_VERSION)
    .then((contract_address) => utils.initContract(contract_address, artifacts.worldNFTV1.abi, readOnly))

const available = (name, chainId) =>
  wrapControllerCall(chainId, 'controller', 'available', [name])

const rentPrice = (name, duration, chainId) =>
  wrapControllerCall(chainId, 'controller', 'rentPrice', [name, duration])

const domainSeparator = (chainId) =>
  wrapControllerCall(chainId, 'controller', 'getDomainSeparator', [])

const makeCommitment = (name, owner, secret, chainId) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((resolver) => wrapControllerCall(chainId, 'controller', 'makeCommitment', [name, owner, secret, resolver, owner]))

const commit = (name, commitment, pass, chainId) =>
  wrapControllerCall(chainId, 'controller', 'whitelistedCommit', [name, commitment, pass])

const register = (name, owner, duration, secret, chainId) =>
  assertContractDeployed(chainId, 'resolver', CURRENT_VERSION)
    .then((resolver) => wrapControllerCall(chainId, 'controller', 'register', [name, owner, duration, secret, resolver, owner]))

const setText = (node, key, value, chainId) =>
  wrapResolverCall(chainId, 'resolver', 'setText', [node, key, value])

const getText = (node, key, chainId) =>
  wrapResolverCall(chainId, 'resolver', 'getText', [node, key])

const roleToSig = (role) => {
  if (role.length == 10 && role.startsWith('0x')) return role
  return ethers.utils.keccak256(ethers.utils.toUtf8Bytes(role)).substr(0, 10)
}

const grantRole = (node, role, addr, chainId) =>
  initResolverContract(chainId, false)
    .then((contract) => contract.setRole(node, roleToSig(role), addr, true))

const revokeRole = (node, role, addr, chainId) =>
  initResolverContract(chainId, false)
    .then((contract) => contract.setRole(node, roleToSig(role), addr, false))

const hasRole = (node, role, addr, chainId) => 
  initResolverContract(chainId, false)
    .then((contract) => contract.hasRole(node, roleToSig(role), addr))

const setNftImage = (node, newImageURL, chainId) =>
  setText(node, 'VEVERSE_WORLD_IMAGE', newImageURL, chainId)

const getNftImage = (node, chainId) =>
  getText(node, 'VEVERSE_WORLD_IMAGE', chainId)

const getNameByNftId = (id, chainId) =>
  wrapNftCall(chainId, 'nft', 'getName', [id])

const getMetadataByNftId = (id, chainId) =>
  wrapNftCall(chainId, 'nft', 'getMetadata', [id])

const getNftsForWallet = (wallet, chainId) =>
  initNftContract(chainId)
    .then((contract) => {
      return Promise.all([
        contract.queryFilter(contract.filters.Transfer(null, wallet)),
        contract.queryFilter(contract.filters.Transfer(wallet))
      ])
    })
    .then(([ins, outs]) => {
      const exclude = outs.reduce((acc, event) => {
        acc[event.topics[3]] = event.blockNumber
        return acc
      }, {})

      return ins.filter((event) => {
        const nftId = event.topics[3]
        return !exclude[nftId] || exclude[nftId] < event.blockNumber
      }).map((event) => event.topics[3])
    })

export {
  utils,
  artifacts,
  contracts,
  assertContractDeployed,
  available,
  rentPrice,
  domainSeparator,
  makeCommitment,
  commit,
  register,
  setText,
  getText,
  roleToSig,
  grantRole,
  revokeRole,
  hasRole,
  setNftImage,
  getNftImage,
  getNameByNftId,
  getMetadataByNftId,
  getNftsForWallet
}