// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import {TestWithDeployments} from "./util/TestWithDeployments.sol";

contract WorldResolverTest is TestWithDeployments {
    uint256 internal constant YEAR = 365 days;

    address internal admin = address(1);
    address internal user = address(2);
    address internal user2 = address(3);

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(user, "User");
        vm.label(user2, "User 2");

        // Setup smart contracts
        vm.startPrank(admin);
        deployAll(admin);
        vm.stopPrank();

        // Register avatar NFT
        vm.startPrank(user);
        registerNFT(user, keccak256("secret"), YEAR, "johndoe");
        vm.stopPrank();
    }

    function testManagingOfRoles() public {
        vm.startPrank(user);
        bytes4 role = bytes4(keccak256("ADMIN"));
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role, user));
        resolver.setRole(nameToNode("johndoe"), role, user, true);
        assertTrue(resolver.hasRole(nameToNode("johndoe"), role, user));
        resolver.setRole(nameToNode("johndoe"), role, user, false);
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role, user));
        vm.stopPrank();
    }

    function testManagingOfSeveralRoles() public {
        vm.startPrank(user);
        bytes4 role1 = bytes4(keccak256("ADMIN"));
        bytes4 role2 = bytes4(keccak256("MANAGER"));
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role1, user));
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role2, user));
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role1, user2));
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role2, user2));
        resolver.setRole(nameToNode("johndoe"), role1, user, true);
        resolver.setRole(nameToNode("johndoe"), role2, user2, true);
        assertTrue(resolver.hasRole(nameToNode("johndoe"), role1, user));
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role2, user));
        assertFalse(resolver.hasRole(nameToNode("johndoe"), role1, user2));
        assertTrue(resolver.hasRole(nameToNode("johndoe"), role2, user2));
        vm.stopPrank();
    }
}
