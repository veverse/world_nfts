// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import {console} from "forge-std/console.sol";
import "@le7el/generative_art/src/SVG9x9.sol";
import "@le7el/generative_art/src/EncodeUtils.sol";
import {WorldMetadataProxyV1} from "src/nft/WorldMetadataProxyV1.sol";
import {TestWithDeployments} from "./util/TestWithDeployments.sol";

contract MetadataTest is TestWithDeployments {
    uint256 internal constant YEAR = 365 days;

    address internal admin = address(1);
    address internal user = address(2);

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(user, "User");

        // Setup smart contracts
        vm.startPrank(admin);
        deployAll(admin);
        WorldMetadataProxyV1 metadata = new WorldMetadataProxyV1();
        controller.changeTokenURIProxy(address(metadata));
        vm.stopPrank();
    }

    function testMetadataValidness() public {
        vm.startPrank(user);
        registerNFT(user, keccak256("secret"), YEAR, "johndoe");
        string memory rawMetadata = nft.tokenURI(86745341786912841616557368118532256523691956314975099975829920571153145112669);
        string memory refMetadata = string(abi.encodePacked(
            "data:application/json;base64,",
            EncodeUtils.base64(bytes('{\"name\":\"johndoe.xr\",\"description\":\"Ownership deed of VeVerse world\",\"image_data\":\"<svg xmlns=\\"http://www.w3.org/2000/svg\\" style=\\"width: 100%; height: 100%\\" viewBox=\\"0 0 9 9\\"><g><rect x=\\"0\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#d65efe\\" /><rect x=\\"1\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"2\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#9803d6\\" /><rect x=\\"3\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"4\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"5\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#ee66b0\\" /><rect x=\\"6\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#e9409b\\" /><rect x=\\"7\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cea\\" /><rect x=\\"8\\" y=\\"0\\" width=\\"1\\" height=\\"1\\" fill=\\"#5eb5fe\\" /><rect x=\\"0\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cea\\" /><rect x=\\"1\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#9803d6\\" /><rect x=\\"2\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"3\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#ee66b0\\" /><rect x=\\"4\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"5\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"6\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#d65efe\\" /><rect x=\\"7\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#27d0e9\\" /><rect x=\\"8\\" y=\\"1\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"0\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"1\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#df47c3\\" /><rect x=\\"2\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"3\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#27d0e9\\" /><rect x=\\"4\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#ee66b0\\" /><rect x=\\"5\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"6\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#5eb5fe\\" /><rect x=\\"7\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#27d0e9\\" /><rect x=\\"8\\" y=\\"2\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"0\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#9803d6\\" /><rect x=\\"1\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#d65efe\\" /><rect x=\\"2\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"3\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#e9409b\\" /><rect x=\\"4\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"5\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"6\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#d65efe\\" /><rect x=\\"7\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#5eb5fe\\" /><rect x=\\"8\\" y=\\"3\\" width=\\"1\\" height=\\"1\\" fill=\\"#00b4ff\\" /><rect x=\\"0\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"1\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"2\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#d65efe\\" /><rect x=\\"3\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#5eb5fe\\" /><rect x=\\"4\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"5\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"6\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#d65efe\\" /><rect x=\\"7\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#2fbadd\\" /><rect x=\\"8\\" y=\\"4\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"0\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#df47c3\\" /><rect x=\\"1\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#1369d3\\" /><rect x=\\"2\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#27d0e9\\" /><rect x=\\"3\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#2fbadd\\" /><rect x=\\"4\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#00b4ff\\" /><rect x=\\"5\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#9803d6\\" /><rect x=\\"6\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#9803d6\\" /><rect x=\\"7\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"8\\" y=\\"5\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"0\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"1\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"2\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#e9409b\\" /><rect x=\\"3\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#2fbadd\\" /><rect x=\\"4\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#2fbadd\\" /><rect x=\\"5\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#00b4ff\\" /><rect x=\\"6\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#e9409b\\" /><rect x=\\"7\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#5eb5fe\\" /><rect x=\\"8\\" y=\\"6\\" width=\\"1\\" height=\\"1\\" fill=\\"#ee66b0\\" /><rect x=\\"0\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#d65efe\\" /><rect x=\\"1\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"2\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#df47c3\\" /><rect x=\\"3\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#5eb5fe\\" /><rect x=\\"4\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"5\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"6\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"7\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"8\\" y=\\"7\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"0\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#2fbadd\\" /><rect x=\\"1\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"2\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#ee66b0\\" /><rect x=\\"3\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#ec2cc6\\" /><rect x=\\"4\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#fe5ec4\\" /><rect x=\\"5\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#df47c3\\" /><rect x=\\"6\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#5ee9fe\\" /><rect x=\\"7\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#e9409b\\" /><rect x=\\"8\\" y=\\"8\\" width=\\"1\\" height=\\"1\\" fill=\\"#2fbadd\\" /></g><text fill=\\"#ffffff\\" x=\\"0.5\\" y=\\"1.5\\" style=\\"font: bold 1pt \'Roboto sans-serif\'\\">johndoe.xr</text></svg>\",\"attributes\":[{\"trait_type\":\"Type\",\"value\":\"VeVerse World\"}]}'))
        ));
        assertTrue(keccak256(bytes(rawMetadata)) == keccak256(bytes(refMetadata)));
        vm.stopPrank();
    }

    function testCustomNFTImages() public {
        vm.startPrank(user);
        registerNFT(user, keccak256("secret"), YEAR, "johndoe");
        resolver.setText(0x777151c19673b9106ec9ea201594cfe56d2d330c0f5ece6cf938ece42f978a1a, "VEVERSE_WORLD_IMAGE", "ipfs://chubaka.jpg");
        string memory rawMetadata = nft.tokenURI(86745341786912841616557368118532256523691956314975099975829920571153145112669);
        string memory refMetadata = string(abi.encodePacked(
            "data:application/json;base64,",
            EncodeUtils.base64(bytes('{\"name\":\"johndoe.xr\",\"description\":\"Ownership deed of VeVerse world\",\"image\":\"ipfs://chubaka.jpg\",\"attributes\":[{\"trait_type\":\"Type\",\"value\":\"VeVerse World\"}]}'))
        ));
        assertTrue(keccak256(bytes(rawMetadata)) == keccak256(bytes(refMetadata)));
        vm.stopPrank();
    }
}