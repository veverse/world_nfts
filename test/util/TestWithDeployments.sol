// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Test.sol";
import "@le7el/web3_crs/contracts/registry/CRSRegistry.sol";
import {WorldControllerV1} from "src/controller/WorldControllerV1.sol";
import {WorldNFTV1} from "src/nft/WorldNFTV1.sol";
import {WorldResolver} from "src/resolver/WorldResolver.sol";

contract TestWithDeployments is Test {
    uint256 internal constant VALIDATOR_KEY = 0x018e912fc034b4200224c4947242dd7c39f65b102bfbed6ea00e6ed6d77bf1f3;

    bytes32 internal l7lBasenode = 0xd0340a34011af087b374bbdc5136a48a841af1b55b0af1143ced23c89cf182c9;
    bytes32 internal l7lHash = 0x37372c33bd16695ac1a42f79a40d119a436fa3a44dc50a9117f2380fa5d80acc;
    bytes32 internal xrBasenode = 0xe110fd4d72744cb6a79e0477d9fe5389d43c86bc065fc9f87570df8590e07285;
    bytes32 internal xrHash = 0x3ea00ad658f4efcf0205eb4b3b3d56898b505e5ca0f0e0f8ffa09f87ce5d88ca;
    CRSRegistry internal crs;
    WorldResolver internal resolver;
    WorldNFTV1 internal nft;
    WorldControllerV1 internal controller;

    address internal validator = vm.addr(VALIDATOR_KEY);

    function deployAll(address admin) internal {
        crs = new CRSRegistry();
        resolver = new WorldResolver(crs);
        nft = new WorldNFTV1(crs, xrBasenode);
        controller = new WorldControllerV1(nft);
        nft.addController(address(controller));
        crs.setSubnodeOwner(bytes32(0), xrHash, admin);
        crs.setSubnodeOwner(bytes32(0), l7lHash, admin);
        crs.setResolver(xrBasenode, address(resolver));
        crs.setResolver(l7lBasenode, address(resolver));
        resolver.setRoyalties(l7lBasenode, address(controller), 0, address(0), address(0)); // free
        resolver.setRoyalties(xrBasenode, address(controller), 3168873850, address(0), address(0)); // 1 gas coin per year
        crs.setSubnodeOwner(bytes32(0), xrHash, address(nft));
        controller.addValidator(validator);
    }

    function registerNFT(address user, bytes32 secret, uint256 duration, string memory name) internal {
        vm.deal(user, 1 ether);
        vm.warp(1657612849);
        bytes32 commitment = controller.makeCommitmentWithConfig(name, user, secret, address(resolver), user);
        controller.commit(name, commitment, whitelistName(name, user));
        vm.warp(block.timestamp + 61);

        // low-level call to pass ether payment
        (bool result,) = address(controller).call{value: 1 ether}(abi.encodeWithSignature(
            "registerWithConfig(string,address,uint256,bytes32,address,address)",
            name, user, duration, secret, address(resolver), user)
        );
        assertTrue(result);
        assertTrue(nft.ownerOf(uint256(keccak256(bytes(name)))) == user);
    }

    function nameToNode(string memory _name) internal view returns (bytes32) {
        bytes32 _label = keccak256(bytes(_name));
        bytes32 _baseNode = controller.baseNode();
        return keccak256(abi.encodePacked(_baseNode, _label));
    }

    function whitelistName(string memory _name, address _user) internal returns (bytes memory) {
        bytes32 _challenge = keccak256(abi.encode(controller.getDomainSeparator(), _user, _name));
        _challenge = keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", _challenge));
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(VALIDATOR_KEY, _challenge);
        return abi.encode(v, r, s);
    }
}